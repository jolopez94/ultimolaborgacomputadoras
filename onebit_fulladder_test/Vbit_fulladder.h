// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _Vbit_fulladder_H_
#define _Vbit_fulladder_H_

#include "verilated.h"
class Vbit_fulladder__Syms;

//----------

VL_MODULE(Vbit_fulladder) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_OUT8(carry_out,0,0);
    //char	__VpadToAlign1[3];
    VL_IN(a,31,0);
    VL_IN(b,31,0);
    VL_OUT(sum,31,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG8(bit_fulladder__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum0__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum0__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum7__DOT__sum0_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum7__DOT__sum1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum7__DOT__sum2_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out,0,0);
    VL_SIG8(bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out,0,0);
    //char	__VpadToAlign82[2];
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum0__sum,3,0);
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum1__sum,3,0);
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum2__sum,3,0);
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum3__sum,3,0);
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum4__sum,3,0);
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum5__sum,3,0);
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum6__sum,3,0);
    VL_SIG8(bit_fulladder__DOT____Vcellout__sum7__sum,3,0);
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign100[4];
    Vbit_fulladder__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    Vbit_fulladder& operator= (const Vbit_fulladder&);	///< Copying not allowed
    Vbit_fulladder(const Vbit_fulladder&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    Vbit_fulladder(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~Vbit_fulladder();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(Vbit_fulladder__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(Vbit_fulladder__Syms* symsp, bool first);
  private:
    static QData	_change_request(Vbit_fulladder__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__1(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__11(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__13(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__15(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__17(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__19(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__21(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__23(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__25(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__27(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__29(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__3(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__31(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__33(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__5(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__7(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_combo__TOP__9(Vbit_fulladder__Syms* __restrict vlSymsp);
  private:
    void	_configure_coverage(Vbit_fulladder__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_eval(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_eval_initial(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_eval_settle(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__10(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__12(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__14(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__16(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__18(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__2(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__20(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__22(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__24(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__26(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__28(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__30(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__32(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__34(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__4(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__6(Vbit_fulladder__Syms* __restrict vlSymsp);
    static void	_settle__TOP__8(Vbit_fulladder__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
