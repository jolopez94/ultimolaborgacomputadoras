// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vbit_fulladder.h for the primary calling header

#include "Vbit_fulladder.h"    // For This
#include "Vbit_fulladder__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(Vbit_fulladder) {
    Vbit_fulladder__Syms* __restrict vlSymsp = __VlSymsp = new Vbit_fulladder__Syms(this, name());
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Vbit_fulladder::__Vconfigure(Vbit_fulladder__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vbit_fulladder::~Vbit_fulladder() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void Vbit_fulladder::eval() {
    Vbit_fulladder__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate Vbit_fulladder::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void Vbit_fulladder::_eval_initial_loop(Vbit_fulladder__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__1(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__1\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (1U & (vlTOPp->a ^ vlTOPp->b)));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1fU));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1eU));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1dU));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1cU));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1bU));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1aU));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x19U));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x18U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x17U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x16U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x15U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x14U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x13U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x12U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x11U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x10U));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xfU));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xeU));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xdU));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xcU));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xbU));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xaU));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 9U));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 8U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 7U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 6U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 5U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 4U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 3U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 2U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out 
	= (1U & (vlTOPp->a & vlTOPp->b));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 1U));
}

void Vbit_fulladder::_settle__TOP__2(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__2\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (1U & (vlTOPp->a ^ vlTOPp->b)));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1fU));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1eU));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1dU));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1cU));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1bU));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x1aU));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x19U));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x18U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x17U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x16U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x15U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x14U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x13U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x12U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x11U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0x10U));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xfU));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xeU));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xdU));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xcU));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xbU));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 0xaU));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 9U));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 8U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 7U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 6U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 5U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 4U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 3U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 2U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out 
	= (1U & (vlTOPp->a & vlTOPp->b));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out 
	= (1U & ((vlTOPp->a ^ vlTOPp->b) >> 1U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 1U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__3(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__3\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 1U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 2U)));
}

void Vbit_fulladder::_settle__TOP__4(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__4\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 2U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum0_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 3U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__5(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__5\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum0_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum0__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 3U)));
    vlTOPp->sum = ((0xfffffff0U & vlTOPp->sum) | (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0_out)));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 4U)));
}

void Vbit_fulladder::_settle__TOP__6(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__6\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xfffffff0U & vlTOPp->sum) | (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum0__sum));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum0_out)));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 4U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 5U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__7(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__7\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 5U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 6U)));
}

void Vbit_fulladder::_settle__TOP__8(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__8\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 6U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum1_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 7U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__9(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__9\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum1_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum1__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 7U)));
    vlTOPp->sum = ((0xffffff0fU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum) 
						  << 4U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1_out)));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 8U)));
}

void Vbit_fulladder::_settle__TOP__10(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__10\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xffffff0fU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum1__sum) 
						  << 4U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum1_out)));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 8U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 9U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__11(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__11\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 9U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xaU)));
}

void Vbit_fulladder::_settle__TOP__12(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__12\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xaU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum2_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0xbU)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__13(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__13\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum2_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum2__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0xbU)));
    vlTOPp->sum = ((0xfffff0ffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum) 
						  << 8U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2_out)));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum2_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xcU)));
}

void Vbit_fulladder::_settle__TOP__14(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__14\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xfffff0ffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum2__sum) 
						  << 8U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum2_out)));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum2_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xcU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xdU)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__15(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__15\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xdU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xeU)));
}

void Vbit_fulladder::_settle__TOP__16(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__16\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0xeU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum3_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0xfU)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__17(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__17\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum3_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum3__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0xfU)));
    vlTOPp->sum = ((0xffff0fffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum) 
						  << 0xcU));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3_out)));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum3_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x10U)));
}

void Vbit_fulladder::_settle__TOP__18(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__18\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xffff0fffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum3__sum) 
						  << 0xcU));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum3_out)));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum3_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x10U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x11U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__19(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__19\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x11U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x12U)));
}

void Vbit_fulladder::_settle__TOP__20(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__20\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x12U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum4_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0x13U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__21(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__21\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum4_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum4__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0x13U)));
    vlTOPp->sum = ((0xfff0ffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum) 
						  << 0x10U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4_out)));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum4_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x14U)));
}

void Vbit_fulladder::_settle__TOP__22(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__22\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xfff0ffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum4__sum) 
						  << 0x10U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum4_out)));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum4_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x14U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x15U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__23(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__23\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x15U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x16U)));
}

void Vbit_fulladder::_settle__TOP__24(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__24\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x16U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum5_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0x17U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__25(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__25\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum5_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum5__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0x17U)));
    vlTOPp->sum = ((0xff0fffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum) 
						  << 0x14U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5_out)));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum5_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x18U)));
}

void Vbit_fulladder::_settle__TOP__26(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__26\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xff0fffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum5__sum) 
						  << 0x14U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum5_out)));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum5_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x18U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x19U)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__27(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__27\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x19U)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1aU)));
}

void Vbit_fulladder::_settle__TOP__28(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__28\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1aU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum6_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0x1bU)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__29(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__29\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->bit_fulladder__DOT__sum6_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out) 
						   & (IData)(vlTOPp->bit_fulladder__DOT__sum6__DOT__sum2_out)) 
						  | ((vlTOPp->a 
						      & vlTOPp->b) 
						     >> 0x1bU)));
    vlTOPp->sum = ((0xf0ffffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum) 
						  << 0x18U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6_out)));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum6_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1cU)));
}

void Vbit_fulladder::_settle__TOP__30(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__30\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xf0ffffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum6__sum) 
						  << 0x18U));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((0xeU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | ((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out) 
	      ^ (IData)(vlTOPp->bit_fulladder__DOT__sum6_out)));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum6_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1cU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1dU)));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__31(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__31\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((0xdU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0_out)) 
	      << 1U));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum0_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1dU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1eU)));
}

void Vbit_fulladder::_settle__TOP__32(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__32\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((0xbU & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1_out)) 
	      << 2U));
    vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2_out 
	= (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out) 
		  & (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum1_out)) 
		 | ((vlTOPp->a & vlTOPp->b) >> 0x1eU)));
    vlTOPp->carry_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out) 
				& (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2_out)) 
			       | ((vlTOPp->a & vlTOPp->b) 
				  >> 0x1fU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2_out)) 
	      << 3U));
}

VL_INLINE_OPT void Vbit_fulladder::_combo__TOP__33(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_combo__TOP__33\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->carry_out = (1U & (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out) 
				& (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2_out)) 
			       | ((vlTOPp->a & vlTOPp->b) 
				  >> 0x1fU)));
    vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum 
	= ((7U & (IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum)) 
	   | (((IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out) 
	       ^ (IData)(vlTOPp->bit_fulladder__DOT__sum7__DOT__sum2_out)) 
	      << 3U));
    vlTOPp->sum = ((0xfffffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum) 
						 << 0x1cU));
}

void Vbit_fulladder::_settle__TOP__34(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_settle__TOP__34\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->sum = ((0xfffffffU & vlTOPp->sum) | ((IData)(vlTOPp->bit_fulladder__DOT____Vcellout__sum7__sum) 
						 << 0x1cU));
}

void Vbit_fulladder::_eval(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_eval\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
    vlTOPp->_combo__TOP__3(vlSymsp);
    vlTOPp->_combo__TOP__5(vlSymsp);
    vlTOPp->_combo__TOP__7(vlSymsp);
    vlTOPp->_combo__TOP__9(vlSymsp);
    vlTOPp->_combo__TOP__11(vlSymsp);
    vlTOPp->_combo__TOP__13(vlSymsp);
    vlTOPp->_combo__TOP__15(vlSymsp);
    vlTOPp->_combo__TOP__17(vlSymsp);
    vlTOPp->_combo__TOP__19(vlSymsp);
    vlTOPp->_combo__TOP__21(vlSymsp);
    vlTOPp->_combo__TOP__23(vlSymsp);
    vlTOPp->_combo__TOP__25(vlSymsp);
    vlTOPp->_combo__TOP__27(vlSymsp);
    vlTOPp->_combo__TOP__29(vlSymsp);
    vlTOPp->_combo__TOP__31(vlSymsp);
    vlTOPp->_combo__TOP__33(vlSymsp);
}

void Vbit_fulladder::_eval_initial(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_eval_initial\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vbit_fulladder::final() {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::final\n"); );
    // Variables
    Vbit_fulladder__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vbit_fulladder::_eval_settle(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_eval_settle\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__2(vlSymsp);
    vlTOPp->_settle__TOP__4(vlSymsp);
    vlTOPp->_settle__TOP__6(vlSymsp);
    vlTOPp->_settle__TOP__8(vlSymsp);
    vlTOPp->_settle__TOP__10(vlSymsp);
    vlTOPp->_settle__TOP__12(vlSymsp);
    vlTOPp->_settle__TOP__14(vlSymsp);
    vlTOPp->_settle__TOP__16(vlSymsp);
    vlTOPp->_settle__TOP__18(vlSymsp);
    vlTOPp->_settle__TOP__20(vlSymsp);
    vlTOPp->_settle__TOP__22(vlSymsp);
    vlTOPp->_settle__TOP__24(vlSymsp);
    vlTOPp->_settle__TOP__26(vlSymsp);
    vlTOPp->_settle__TOP__28(vlSymsp);
    vlTOPp->_settle__TOP__30(vlSymsp);
    vlTOPp->_settle__TOP__32(vlSymsp);
    vlTOPp->_settle__TOP__34(vlSymsp);
}

VL_INLINE_OPT QData Vbit_fulladder::_change_request(Vbit_fulladder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_change_request\n"); );
    Vbit_fulladder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void Vbit_fulladder::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_ctor_var_reset\n"); );
    // Body
    a = VL_RAND_RESET_I(32);
    b = VL_RAND_RESET_I(32);
    sum = VL_RAND_RESET_I(32);
    carry_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT____Vcellout__sum0__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT____Vcellout__sum1__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT____Vcellout__sum2__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT____Vcellout__sum3__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT____Vcellout__sum4__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT____Vcellout__sum5__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT____Vcellout__sum6__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT____Vcellout__sum7__sum = VL_RAND_RESET_I(4);
    bit_fulladder__DOT__sum0__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum0__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum0__DOT__sum0__DOT__a2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum0__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum0__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum0__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1__DOT__sum0__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum1__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2__DOT__sum0__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum2__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3__DOT__sum0__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum3__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4__DOT__sum0__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum4__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5__DOT__sum0__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum5__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6__DOT__sum0__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum6__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum7__DOT__sum0_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum7__DOT__sum1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum7__DOT__sum2_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum7__DOT__sum0__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum7__DOT__sum1__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum7__DOT__sum2__DOT__x1_out = VL_RAND_RESET_I(1);
    bit_fulladder__DOT__sum7__DOT__sum3__DOT__x1_out = VL_RAND_RESET_I(1);
}

void Vbit_fulladder::_configure_coverage(Vbit_fulladder__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    Vbit_fulladder::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
