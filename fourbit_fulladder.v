module fourbit_fulladder(
    input [3:0] a,
    input [3:0] b,
    input carry_in,
    output [3:0] sum,
    output carry_out
    );

    wire sum0_out;
    wire sum1_out;
    wire sum2_out;
    
    onebit_fulladder sum0(.a(a[0]), .b(b[0]), .carry_in(carry_in), .sum(sum[0]), .carry_out(sum0_out));
    onebit_fulladder sum1(.a(a[1]), .b(b[1]), .carry_in(sum0_out), .sum(sum[1]), .carry_out(sum1_out));
    onebit_fulladder sum2(.a(a[2]), .b(b[2]), .carry_in(sum1_out), .sum(sum[2]), .carry_out(sum2_out));
    onebit_fulladder sum3(.a(a[3]), .b(b[3]), .carry_in(sum2_out), .sum(sum[3]), .carry_out(carry_out));
endmodule