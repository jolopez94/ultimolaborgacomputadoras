module bit_fulladder(
    input [31:0] a,
    input [31:0] b,
    output [31:0] sum,
    output carry_out
    );

    wire sum0_out;
    wire sum1_out;
    wire sum2_out;
    wire sum3_out;
    wire sum4_out;
    wire sum5_out;
    wire sum6_out;

    fourbit_fulladder sum0(.a(a[3:0]), .b(b[3:0]), .carry_in(0), .sum(sum[3:0]), .carry_out(sum0_out));
    fourbit_fulladder sum1(.a(a[7:4]), .b(b[7:4]), .carry_in(sum0_out), .sum(sum[7:4]), .carry_out(sum1_out));
    fourbit_fulladder sum2(.a(a[11:8]), .b(b[11:8]), .carry_in(sum1_out), .sum(sum[11:8]), .carry_out(sum2_out));
    fourbit_fulladder sum3(.a(a[15:12]), .b(b[15:12]), .carry_in(sum2_out), .sum(sum[15:12]), .carry_out(sum3_out));
    fourbit_fulladder sum4(.a(a[19:16]), .b(b[19:16]), .carry_in(sum3_out), .sum(sum[19:16]), .carry_out(sum4_out));
    fourbit_fulladder sum5(.a(a[23:20]), .b(b[23:20]), .carry_in(sum4_out), .sum(sum[23:20]), .carry_out(sum5_out));
    fourbit_fulladder sum6(.a(a[27:24]), .b(b[27:24]), .carry_in(sum5_out), .sum(sum[27:24]), .carry_out(sum6_out));
    fourbit_fulladder sum7(.a(a[31:28]), .b(b[31:28]), .carry_in(sum6_out), .sum(sum[31:28]), .carry_out(carry_out));

endmodule